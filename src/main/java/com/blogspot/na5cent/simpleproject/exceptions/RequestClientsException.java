/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.exceptions;

/**
 *
 * @author redcorw
 */
public class RequestClientsException extends Exception {

    public RequestClientsException() {
        super();
    }

    public RequestClientsException(String message) {
        super(message);
    }

    public RequestClientsException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestClientsException(Throwable cause) {
        super(cause);
    }
}
