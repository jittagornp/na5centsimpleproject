/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.services.implementations;

import com.blogspot.na5cent.simpleproject.models.User;
import com.blogspot.na5cent.simpleproject.repositories.UserRepository;
import com.blogspot.na5cent.simpleproject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author redcorw
 */
@Service
@Transactional(propagation= Propagation.NOT_SUPPORTED)
public class UserServiceImplementation implements UserService{

    @Autowired
    private UserRepository userRepository;
    
    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByUsernameAndPassword(String username, String password) {            
        return userRepository.findByUsernameAndPassword(username, password);
    }
    
}
