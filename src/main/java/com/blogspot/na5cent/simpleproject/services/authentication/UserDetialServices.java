/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.services.authentication;

import com.blogspot.na5cent.simpleproject.enumerations.AuthorityType;
import com.blogspot.na5cent.simpleproject.models.User;
import com.blogspot.na5cent.simpleproject.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 *
 * @author redcorw
 */
public class UserDetialServices implements UserDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(UserDetialServices.class);
    @Autowired
    private UserService userService;
    
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOG.info("try to log in with account '{}'", username);
        User user = userService.findByUsername(username);
//        
//        
//        user.setUsername(username);
//        user.setPassword("1234");
//        if("admin".equals(username)){
//            user.setAuthorityType(AuthorityType.ADMIN);
//        }else if("editor".equals(username)){
//            user.setAuthorityType(AuthorityType.EDITOR);
//        }else{
//            user.setAuthorityType(AuthorityType.PUBLIC);
//        }
//        
//        
        if (user == null) {
            LOG.info("Not found username ={} ", username);
            throw new UsernameNotFoundException("Login incorrect.");
        }
        LOG.info("found usename '{}' in database.", username);
        return new SimpleProjectUserDetials(user);
    }
}
