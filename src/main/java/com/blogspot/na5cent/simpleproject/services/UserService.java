/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.services;

import com.blogspot.na5cent.simpleproject.models.User;

/**
 *
 * @author redcorw
 */
public interface UserService {
    public User save(User user);
    
    public User findByUsername(String username);
    
    public User findByUsernameAndPassword(String username, String password);
}
