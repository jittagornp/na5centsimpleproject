/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.services.authentication;

import com.blogspot.na5cent.simpleproject.enumerations.AuthorityType;
import com.blogspot.na5cent.simpleproject.models.User;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author redcorw
 */
public class SimpleProjectUserDetials implements UserDetails {

    public static final GrantedAuthority ADMIN_ROLE = new SimpleGrantedAuthority("ADMIN_ROLE");
    public static final GrantedAuthority EDITOR_ROLE = new SimpleGrantedAuthority("EDITOR_ROLE");
    public static final GrantedAuthority PUBLIC_ROLE = new SimpleGrantedAuthority("PUBLIC_ROLE");
    private User user;
    private Set<GrantedAuthority> grants;

    SimpleProjectUserDetials(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (grants == null) {
            grants = new HashSet<GrantedAuthority>();
            if (user.getAuthority() == AuthorityType.ADMIN) {
                grants.add(ADMIN_ROLE);
            } else if (user.getAuthority() == AuthorityType.EDITOR) {
                grants.add(EDITOR_ROLE);
            } else {
                grants.add(PUBLIC_ROLE);
            }
        }
        return grants;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
