/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.enumerations;

/**
 *
 * @author redcorw
 */
public enum AuthorityType {

    ADMIN("ผู้ดูแลระบบ"),
    EDITOR("แก้ไข"),
    PUBLIC("สาธารณะ");
    private String description;

    private AuthorityType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
