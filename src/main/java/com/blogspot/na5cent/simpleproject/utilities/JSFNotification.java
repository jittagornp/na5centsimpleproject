/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.utilities;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;


/**
 *
 * @author redcorw
 */
public class JSFNotification {
    public static final Severity INFO = FacesMessage.SEVERITY_INFO;
    public static final Severity WARN = FacesMessage.SEVERITY_WARN;
    public static final Severity ERROR = FacesMessage.SEVERITY_ERROR;
    
    public static void notifyClient(Severity notificationType, String title, String body){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(notificationType, title, body));
    }
}
