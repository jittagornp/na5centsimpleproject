/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.utilities;

import com.blogspot.na5cent.simpleproject.exceptions.RequestClientsException;
import javax.faces.context.FacesContext;

/**
 *
 * @author redcorw
 */
public class JSFRequest {

    /**
     * get request query string(parameter String) from user request to Integer
     * value,
     *
     * Example try{ Integer request = requestInteger("userId");
     * }catch(RequestClientsException ex){ LOG.debug("request client error{}",
     * ex.getMessage()); }
     *
     * @param id
     * @return Integer.valueOf(id)
     * @throws RequestClientsException
     */
    public static Integer requestInteger(String id) throws RequestClientsException {
        id = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get(id);
        if (id == null) {
            throw new RequestClientsException("can't get parameter map for user, result is null");
        }
        return Integer.valueOf(id);
    }

    /**
     * get request query string(parameter String) from user request to String
     * value
     *
     * @param id
     * @return id
     * @throws RequestClientsException
     */
    public static String requestString(String id) throws RequestClientsException {
        id = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get(id);
        if (id == null) {
            throw new RequestClientsException("can't get parameter map for user, result is null");
        }
        return id;
    }

    /**
     * get request query string(parameter String) from user request to Boolean
     * value
     *
     * @param id
     * @return Boolean.valueOf(id);
     * @throws RequestClientsException
     */
    public static Boolean requestBoolean(String id) throws RequestClientsException {
        id = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get(id);
        if (id == null) {
            throw new RequestClientsException("can't get parameter map for user, result is null");
        }
        return Boolean.valueOf(id);
    }
}
