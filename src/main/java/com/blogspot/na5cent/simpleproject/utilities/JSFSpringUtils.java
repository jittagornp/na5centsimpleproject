/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.utilities;

import javax.servlet.ServletContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author redcorw
 */
public class JSFSpringUtils {

    public static <T> T getBean(Class<T> clazz) {
        return WebApplicationContextUtils.getWebApplicationContext(JSFUtils.getServletContext()).getBean(clazz);
    }

    public static Object getBean(String bean) {
        return WebApplicationContextUtils.getWebApplicationContext(JSFUtils.getServletContext()).getBean(bean);
    }

    public static <T> T getBean(ServletContext servletContext, Class<T> clazz) {
        return WebApplicationContextUtils.getWebApplicationContext(servletContext).getBean(clazz);
    }

    public static Object getBean(ServletContext servletContext, String bean) {
        return WebApplicationContextUtils.getWebApplicationContext(servletContext).getBean(bean);
    }
}
