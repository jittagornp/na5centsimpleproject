/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.utilities;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.web.jsf.FacesContextUtils;

/**
 *
 * @author redcorw
 */
public class JSFUtils {

    public static ServletContext getServletContext() {
        return FacesContextUtils
                .getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
                .getServletContext();
    }
}
