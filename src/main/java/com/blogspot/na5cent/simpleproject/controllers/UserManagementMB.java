/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.controllers;

import com.blogspot.na5cent.simpleproject.enumerations.AuthorityType;
import com.blogspot.na5cent.simpleproject.models.User;
import com.blogspot.na5cent.simpleproject.services.UserService;
import com.blogspot.na5cent.simpleproject.utilities.JSFSpringUtils;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author redcorw
 */
@ManagedBean
@RequestScoped
public class UserManagementMB {

    private UserService userService;

    @PostConstruct
    public void postConstruct() {
        userService = JSFSpringUtils.getBean(UserService.class);
    }

    public String save() {
         User user = userService.findByUsernameAndPassword("admin", "1234");
        return user.getId() + " : " + user.getAuthority().getDescription();
    }
}
