/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.repositories;

import com.blogspot.na5cent.simpleproject.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author redcorw
 */
public interface UserRepository extends JpaRepository<User, Integer> {
    public User findByUsername(String username);
    
    //@Query("SELECT us FROM User us WHERE us.username = ?1 AND us.password = ?2")
    public User findByUsernameAndPassword(String username, String password);
    
    
    @Modifying
    @Query("UPDATE User us SET us.password = ?1 WHERE us = ?2")
    public void updatePasswordByUser(String newPassword, User user); 
}
