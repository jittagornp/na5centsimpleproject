/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author redcorw
 */
@Entity
@ReadOnly
@Table(name = "SIMPLE_STAFF")
public class Staff implements Serializable {

    @Id
    @Column(name = "SRAFF_ID", length = 12)
    private String staffId;
    @Column(name = "STAFF_NAME")
    private String staffName;

    public Staff() {
    }

    public Staff(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + (this.staffId != null ? this.staffId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Staff other = (Staff) obj;
        if ((this.staffId == null) ? (other.staffId != null) : !this.staffId.equals(other.staffId)) {
            return false;
        }
        return true;
    }
}
