/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.na5cent.simpleproject.models;

import com.blogspot.na5cent.simpleproject.enumerations.AuthorityType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author redcorw
 */
@Entity
@Table(name = "USER_SIMPLE_PROJECT")
public class User implements Serializable {

    @Id
    @TableGenerator(name = "User_GEN",
    table = "SIMPLE_PROJECT_SEQ",
    pkColumnName = "NAME",
    valueColumnName = "VALUE",
    pkColumnValue = "USER_SIMPLE_PROJECT")
    @GeneratedValue(generator = "User_GEN",
    strategy = GenerationType.TABLE)
    private Integer id;
    @Column(length = 32)
    private String username;
    private String password;
    private AuthorityType authority;

    public User() {
    }

    public User(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        if (username == null) {
            username = "";
        }
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        if (password == null) {
            password = "";
        }
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AuthorityType getAuthority() {
        if (authority == null) {
            authority = AuthorityType.PUBLIC;
        }
        return authority;
    }

    public void setAuthorityType(AuthorityType authority) {
        this.authority = authority;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
